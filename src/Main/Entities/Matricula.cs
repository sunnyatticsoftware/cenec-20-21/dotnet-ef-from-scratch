﻿namespace Main.Entities
{
    public class Matricula
    {
        public int Id { get; set; }
        public Alumno Alumno { get; set; }
        public int AlumnoId { get; set; }
        public Asignatura Asignatura { get; set; }
        public int AsignaturaId { get; set; }
    }
}