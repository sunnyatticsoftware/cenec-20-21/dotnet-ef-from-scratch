﻿using System.Collections.Generic;

namespace Main.Entities
{
    public class Asignatura
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public List<Matricula> Matriculas { get; set; }
        // public Profesor Profesor { get; set; }
        // public int ProfesorId { get; set; }
    }
}